package main.Interpolation;

import main.Function;
import main.Polynom;

import java.util.*;

/**
 * Created by Саша on 04.04.2016.
 * Интерполирование по равноотстоящим узлам
 */
public class OtherInterpolation {
    private List<Point> points;
    private List<List<Double>> endDiff;
    private Polynom polynom;
    private Polynom w;
    private Function f;
    private double h;

    public static void test(Function fun){
        System.out.println("Интерполирвоание по равноотстоящим узлам.");
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите количество точек в таблице:");
        int m = sc.nextInt();
        System.out.println("Введите границы промежутка:");
        double a = sc.nextDouble();
        double b = sc.nextDouble();
        List<Point> table = generateList(m, a, b, fun);
        for(Point p : table)
            System.out.println(p);
        System.out.print("Введите степень интерполяционного многочлена (? < " + m + "):");
        int n;
        while(true){
            n = sc.nextInt();
            if(n >= m)
                System.out.println("Попробуйте еще раз, значение должно быть меньше " + m);
            else
                break;
        }
        double h = (b - a) / m;
        double l = (((a + b) / 2) - h*((n - 1)/2));
        double r = (((a + b) / 2) + h*((n - 1)/2));
        System.out.println("Точка, в которой ищем значение");
        System.out.println("Точка должна лежать в промежутках ["
                + a + ", " + (a + h) + "], [" + (b - h) + ", " + b + "] или ["
                + l + ", " + r + "]");
        double x;
        while(true){
            x = sc.nextDouble();
            if(!((x >= a && x <= a + h) || (x >= b - h && x <= b) || (x >= l && x <= r)))
                System.out.println("Попробуйте еще раз, значение должно лежать в промежутках ["
                        + a + ", " + (a + h) + "], [" + (b - h) + ", " + b + "] или ["
                        + l + ", " + r + "]");
            else
                break;
        }
        OtherInterpolation inter = new OtherInterpolation(table, fun);
        Polynom polynom = inter.getPolynom(x, n);
        System.out.println("Значение в заданной точке: " + polynom.getValue(x));
        System.out.println("Абсолютная погрешность: " + Math.abs(polynom.getValue(x) - fun.getValue(x)));
    }

    public OtherInterpolation(List<Point> points, Function fun){
        f = fun;
        this.points = points;
        h = points.get(1).x - points.get(0).x;
        endDiff = new ArrayList<>();
        polynom = new Polynom();
        w = new Polynom(1.0);
        sortPoints();
        //init();
    }

    public Polynom getPolynom(double x, int n){
        polynom = new Polynom(0.0);
        w = new Polynom(1.0);
        makePolynom(x, n);
        return polynom;
    }

    private void makePolynom(double x, int n){
        if(x <= points.get(1).x/* && x >= points.get(0).x*/)
            firstSegment(x, n);
        else if(/*x <= points.get(points.size() - 1).x && */x >= points.get(points.size() - 2).x)
            lastSegment(x, n);
        else {
            //gauss(x, n);
            stupidGauss(x, n);
        }
    }

    private void firstSegment(double x, int n){
        for (int i = 0; i <= n; i++){
            polynom = polynom.add(w.mult(getDelta(i, points.get(0).x)).div(Math.max(i, 1)));
            w = w.mult(new Polynom(Arrays.asList((-1.0)*(i + points.get(0).x/h), 1.0/h))).div(Math.max(i, 1));
        }
    }

    private void lastSegment(double x, int n){
        int m = points.size() - 1;
        for (int k = 0; k < n + 1; k++){
            polynom = polynom.add(w.mult(getDelta(k, points.get(m - k).x)).div(Math.max(k, 1)));
            w = w.mult(new Polynom(Arrays.asList((1.0)*(k - points.get(m).x/h), 1.0/h))).div(Math.max(k, 1));
        }
    }

    private void gauss(double x, int n){
        int ind = 0;
        for(int i = 0; i < points.size() - 1; i++)
            if(x > points.get(i).x - (h/2) && x <= points.get(i + 1).x - (h/2)) {
                ind = i;
                break;
            }
        /*if(Math.abs(points.size() - ind) < n / 2 || ind < n / 2){
            System.out.println("bad point!");
            return;
        }*/

        Polynom p = new Polynom(1.0); //он просто для проверки
        polynom = polynom.add(getDelta(0, points.get(ind).x));
        System.out.println("В первый раз: " + points.get(ind).x);
        for (int i = 1; i <= n; i++){
            /*System.out.println("polynom = " + polynom + ", w = " + w);
            System.out.println(getDelta(i, points.get(ind - i / 2).x));
            System.out.println("P = " + p);*/
            //w = w.mult(new Polynom(Arrays.asList(Math.pow(-1.0, i)*(i / 2), 1.0))).div(i);
            w = w.mult(new Polynom(Arrays.asList(Math.pow(-1.0, (i + 1))*((i) / 2) - points.get(ind).x/h, x/h))).div(i); //до сегодняшнего дня
            //w = w.mult(new Polynom(Arrays.asList(Math.pow(-1.0, (i + 1))*((i) / 2) + points.get(ind).x, h))).div(i); //новое
            p = p.mult((new Polynom(Arrays.asList(Math.pow(-1.0, i)*(i / 2), 1.0))));
            polynom = polynom.add(w.mult(getDelta(i, points.get(ind - i / 2).x)));
            //System.out.println("Point: " + points.get(ind - i / 2).x);
        }
        System.out.println("w = " + w.allString());
        System.out.println("polynom = " + polynom.allString());
    }

    private void stupidGauss(double x, int n){
        int ind = 0;
        for(int i = 0; i < points.size() - 1; i++)
            if(x > points.get(i).x - (h/2) && x <= points.get(i + 1).x - (h/2)) {
                ind = i;
                break;
            }
        double wr = 1;
        double ans = points.get(ind).y;
        double t = (x - points.get(ind).x) / h;
        for (int i = 1; i <= n; i++){
            wr *= (t - Math.pow(-1, i)*((i) / 2));
            wr /= i;
            ans += wr * getDelta(i, points.get(ind - i / 2).x);
        }
        polynom = new Polynom(ans);
    }

    //заполняет таблицу разделенных разностей
    private void init(){
        endDiff = new ArrayList<>();
        endDiff.add(new ArrayList<>()); //столбец аргументов
        endDiff.add(new ArrayList<>()); //столбец значений
        for(Point p : points){
            endDiff.get(0).add(p.x);
            endDiff.get(1).add(p.y);
        }
        int n = points.size();
        for(int i = n; i >= 0; i--){
            endDiff.add(new ArrayList<>());
            for(int j = 0; j < i ; i++){
                endDiff.get(2 + n - i).add(j, endDiff.get(1 + n - i).get(j + 1) - endDiff.get(1 + n - i).get(j));
            }
        }
        for(List<Double> list : endDiff){
            for(double r : list)
                System.out.print(r + " ");
            System.out.println();
        }
    }

    private double getDelta(int k, double x){
        if(k == 0)
            return f.getValue(x);
        return getDelta(k - 1, x + h) - getDelta(k - 1, x);
    }

    //пока не тот
    private double getNextCoeff(Point p){
        //points.add(p);
        endDiff.get(0).add(p.x); //новый аргумент
        endDiff.get(1).add(p.y); //новое значение
        if(endDiff.get(0).size() == 1)
            return p.y;
        endDiff.add(new ArrayList<Double>()); //добавили столбец для последнего
        double fn1, fn2, x1, x2;
        for(int i = 2; i < endDiff.size(); i++) {
            fn1 = endDiff.get(i - 1).get(endDiff.get(i - 1).size() - 2);
            fn2 = endDiff.get(i - 1).get(endDiff.get(i - 1).size() - 1);
            x1 = endDiff.get(0).get(endDiff.get(i - 1).size() - 2);
            x2 = endDiff.get(0).get(endDiff.get(0).size() - 1);
            endDiff.get(i).add((fn2 - fn1) / (x2 - x1));
        }
        return endDiff.get(endDiff.size() - 1).get(0);
    }

    private static ArrayList<Point> generateList(int m, double a, double b, Function f){
        double h = (b - a) / m;
        ArrayList<Point> points = new ArrayList<>();
        for(int i = 0; i < m; i++)
            points.add(new Point(a + i*h, f.getValue(a + i*h)));
        return points;
    }

    private void sortPoints(){
        Collections.sort(points, new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
                if (o1.x - o2.x > 0)
                    return 1;
                else if (o1.x - o2.x == 0)
                    return 0;
                else
                    return -1;
            }
        });
    }
}
