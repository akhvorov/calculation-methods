package main.Interpolation;

import main.Polynom;

import java.util.*;

/**
 * Created by Саша on 08.03.2016.
 */
public interface Interpolation {
    void addPoints(List<Point> points);
    void addPoint(Point point);
    Polynom getPolynom();
    String getName();
}
