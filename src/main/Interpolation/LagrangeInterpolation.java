package main.Interpolation;

import main.Polynom;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Саша on 08.03.2016.
 */
public class LagrangeInterpolation implements Interpolation {
    Polynom polynom;
    List<Point> points;

    public LagrangeInterpolation(List<Point> points){
        this.points = points;
        polynom = new Polynom(0.0);
        for(int i = 0; i < points.size(); i++)
            polynom = polynom.add(fundamentPoly(i).mult(this.points.get(i).y));
    }

    private Polynom fundamentPoly(int k){
        Polynom p = calcW(k);
        return p.div(p.getValue(points.get(k).x));
    }

    private Polynom calcW(int k){
        Polynom p = new Polynom(1.0);
        for(int i = 0; i < points.size(); i++)
            if(i != k)
                p = p.mult(new Polynom(Arrays.asList((-1.0)*points.get(i).x, 1.0)));
        return p;
    }

    @Override
    public void addPoints(List<Point> points) {
        this.points.addAll(points);
        polynom = new Polynom();
        for(int i = 0; i < points.size(); i++)
            polynom = polynom.add(fundamentPoly(i).mult(points.get(i).y));
    }

    @Override
    public void addPoint(Point point) {
        this.points.add(point);
        polynom = new Polynom();
        for(int i = 0; i < points.size(); i++)
            polynom = polynom.add(fundamentPoly(i).mult(points.get(i).y));
    }

    @Override
    public Polynom getPolynom() {
        return polynom;
    }

    @Override
    public String getName() {
        return "Лагранж";
    }
}
