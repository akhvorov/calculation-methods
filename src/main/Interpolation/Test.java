package main.Interpolation;

import main.Function;

import java.util.*;

/**
 * Created by Саша on 23.03.2016.
 */
public class Test{
    public static void interpolationTest(){
        System.out.println("Задача алгебраического интерполирования." + "\n" +
                "Интерполяционный многочлен в форме Ньютона и в форме Лагранжа");
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите количество точек в таблице:");
        int m = sc.nextInt();
        List<Point> table = generateList(m + 1);
        for(Point p : table)
            System.out.println(p);
        System.out.print("Введите степень интерполяционного многочлена (? < " + m + ") :");
        int n;
        while(true){
            n = sc.nextInt();
            if(n >= m)
                System.out.println("Попробуйте еще раз, значение должно быть меньше " + m);
            else
                break;
        }
        System.out.print("Точка, в которой ищем значение:");
        double x = sc.nextDouble();
        List<Point> points = getNearestPoints(n + 1, table, x);
        System.out.println(points);
        System.out.println("Значения в точках:");
        for(Point p : points)
            System.out.println(p);
        System.out.println("\n" + "Многочлены в формах:");
        List<Interpolation> interpolations = Arrays.asList(new LagrangeInterpolation(points), new NewtownInterpolation(points));
        for (Interpolation interpolation : interpolations)
            System.out.println(interpolation.getName() + ": " + interpolation.getPolynom());
        System.out.println("Значения многочленов в заданной точке:");
        for(Interpolation interpolation : interpolations)
            System.out.println(interpolation.getName() + ": " + (interpolation.getPolynom().getValue(x)));

        System.out.println("Погрешность:");
        for(Interpolation interpolation : interpolations)
            System.out.println(interpolation.getName() + ": " + (new InterFun().getValue(x) - interpolation.getPolynom().getValue(x)));
    }

    private static List<Point> generateList(int n){
        Random ran = new Random(5);
        Set<Double> set = new HashSet<>();
        while(set.size() != n)
            set.add(ran.nextDouble()*10 - 5);
        List<Point> points = new ArrayList();
        for(Double x : set)
            points.add(new Point(x, new InterFun().getValue(x)));
            /*points.add(new Point(-1, -13));
            points.add(new Point(0, -5));
            points.add(new Point(1, -3));
            points.add(new Point(2, -1));*/
        return points;
    }

    private static List<Point> getNearestPoints(int n, List<Point> points, final double x){
        List<Point> list = new ArrayList<Point>();
        Collections.sort(points, new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
                double n = Math.abs(o1.x - x);
                double t = Math.abs(o2.x - x);
                if(n - t > 0)
                    return 1;
                else if(n - t == 0)
                    return 0;
                else
                    return -1;
            }
        });
        for(int i = 0; i < n; i++){
            list.add(points.get(i));
        }
        return list;
    }
}


class InterFun implements Function {
    public double getValue(double x){
        //return Math.pow(Math.cos(x), 3) + x*x*x - 4*x*x - Math.exp(x);
        return x*x*x - 4*x*x - 2*x - 9;
    }
}
