package main.Interpolation;

import main.Polynom;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Саша on 08.03.2016.
 */
public class NewtownInterpolation implements Interpolation {
    Polynom polynom;
    Polynom w;
    List<Point> points;
    List<List<Double>> rasDiff;

    public NewtownInterpolation(List<Point> points){
        this.points = points;
        polynom = new Polynom();
        w = new Polynom(1.0);
        Init();
    }

    private void Init(){
        rasDiff = new ArrayList<>();
        rasDiff.add(new ArrayList<>()); //столбец аргументов
        rasDiff.add(new ArrayList<>()); //столбец значений
        for(Point p : points){
            double g = getNextCoeff(p);
            polynom = polynom.add(w.mult(g));
            w = w.mult(new Polynom(Arrays.asList((-1.0)*p.x, 1.0)));
        }
    }

    private double getNextCoeff(Point p){
        //points.add(p);
        rasDiff.get(0).add(p.x); //новый аргумент
        rasDiff.get(1).add(p.y); //новое значение
        if(rasDiff.get(0).size() == 1)
            return p.y;
        rasDiff.add(new ArrayList<Double>()); //добавили столбец для последнего
        double fn1, fn2, x1, x2;
        for(int i = 2; i < rasDiff.size(); i++) {
            fn1 = rasDiff.get(i - 1).get(rasDiff.get(i - 1).size() - 2);
            fn2 = rasDiff.get(i - 1).get(rasDiff.get(i - 1).size() - 1);
            x1 = rasDiff.get(0).get(rasDiff.get(i - 1).size() - 2);
            x2 = rasDiff.get(0).get(rasDiff.get(0).size() - 1);
            rasDiff.get(i).add((fn2 - fn1) / (x2 - x1));
        }
        return rasDiff.get(rasDiff.size() - 1).get(0);
    }

    public void printTable(){
        for(int i = 0; i < rasDiff.size(); i++) {
            for (int j = 0; j < rasDiff.get(i).size(); j++)
                System.out.print(rasDiff.get(i).get((j)) + " ");
            System.out.println();
        }
    }

    @Override
    public void addPoints(List<Point> points) {
        for(Point p : points){
            points.add(p);
            polynom = polynom.add(w.mult(getNextCoeff(p)));
            w = w.mult(new Polynom(Arrays.asList((-1.0)*p.x, 1.0)));
        }
    }

    @Override
    public void addPoint(Point point) {
        points.add(point);
        polynom = polynom.add(w.mult(getNextCoeff(point)));
        w = w.mult(new Polynom(Arrays.asList((-1.0)*point.x, 1.0)));
    }

    @Override
    public Polynom getPolynom() {
        //printTable();
        return polynom;
    }
    @Override
    public String getName() {
        return "Ньютон";
    }
}
