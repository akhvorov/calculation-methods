package main;

import java.util.*;

/**
 * Приближенное вычисление интегралов при помощи Квадратурных Формул Наивысшей Алгебраической Степени Точности
 */
public class KFNAST {
    public static void test(){
        Function f = new Function() {
            @Override
            public double getValue(double x) {
                return Math.sin(x);
            }
        };
        Function w = new Function() {
            @Override
            public double getValue(double x) {
                return Math.pow(x, 0.5);
            }
        };
        Function fi = new Function() {
            @Override
            public double getValue(double x) {
                return f.getValue(x)*w.getValue(x);
            }
        };
        int N = 2;
        double a, b;
        Scanner sc = new Scanner(System.in);
        /*a = sc.nextDouble();
        b = sc.nextDouble();*/
        a = 0.0;
        b = 1.0;
        int m;
        //m = sc.nextInt();
        m = 100;
        Segment segment = new Segment(a, b);
        double gaussKF = gaussKF(fi, segment, m, N);
        double gaussNAST = gaussNAST(f, w, segment, m);
        double integral = Integral.integrate(fi, segment, m, Integral.IntegralType.AVERAGE_RECTANGLE);
        System.out.println("gaussKF = " + gaussKF);
        System.out.println("gaussNAST = " + gaussNAST);
        System.out.println("Средние прямоугольники = " + integral);
    }

    public static double gaussNAST(Function f, Function w, Segment segment, int m){
        int N = 2;
        double y0, y1, y2, y3;
        y0 = Integral.integrate(w, segment, 500, Integral.IntegralType.AVERAGE_RECTANGLE);
        y1 = Integral.integrate(new Function() {
            @Override
            public double getValue(double x) {
                return w.getValue(x)*x;
            }
        }, segment, 500, Integral.IntegralType.AVERAGE_RECTANGLE);
        y2 = Integral.integrate(new Function() {
            @Override
            public double getValue(double x) {
                return w.getValue(x)*x*x;
            }
        }, segment, 500, Integral.IntegralType.AVERAGE_RECTANGLE);
        y3 = Integral.integrate(new Function() {
            @Override
            public double getValue(double x) {
                return w.getValue(x)*x*x*x;
            }
        }, segment, 500, Integral.IntegralType.AVERAGE_RECTANGLE);
        //System.out.println(y0 + ", " + y1 + ", " + y2 + ", " + y3);
        double delta = y1*y1 - y2*y0, delta1 = y0*y3 - y1*y2, delta2 = y2*y2 - y1*y3;
        double c1 = delta1/delta, c2 = delta2 / delta;
        Double[] c = quadratic(c1, c2);
        double x1 = c[0], x2 = c[1];
        Polynom w2 = new Polynom(Arrays.asList(new Double[]{c2, c1, 1.0}));
        delta = x2 - x1;
        delta1 = x2*y0 - y1;
        delta2 = y1 - x1*y0;
        double A1 = delta1 / delta, A2 = delta2 / delta;
        //System.out.println("Проверка: " + y2 + " = " + (A1*x1*x1 + A2*x2*x2));
        //System.out.println("Проверка: " + y3 + " = " + (A1*x1*x1*x1 + A2*x2*x2*x2));
        System.out.println("Моменты: " + y0 + ", " + y1 + ", " + y2 + ", " + y3);
        System.out.println("Ортогональный многочлен: " + w2);
        System.out.println("Коэффициенты: " + A1 + ", " + A2);
        return (A1 * f.getValue(x1) + A2 * f.getValue(x2));
    }

    private static Double[] quadratic(double b, double c){
        double d = Math.pow(b*b - 4*c, 0.5);
        Double[] r = new Double[]{(-b - d) / 2, (-b + d) / 2};
        return r;
    }

    public static double gaussKF(Function f, Segment segment, int m, int N){
        double ans = 0;
        List<Double> roots;
        //roots = legandr(N).roots();
        roots = Arrays.asList(new Double[]{-1*Math.pow(3, -1/3), Math.pow(3, -1/3)});
        double h = segment.lenght() / m;
        for(int i = 0; i < m; i++){
            double z = segment.l + ((2*i + 1) * segment.lenght() / (2*m));
            for(Double t : roots)
                ans += (h / 2) * (f.getValue(t * h/2 + z));
        }
        return ans;
    }

    private static Polynom legandr(int n){
        if(n == 0)
            return new Polynom(1.0);
        if(n == 1)
            return new Polynom(Arrays.asList(new Double[]{0.0, 1.0}));
        double t = (double)n;
        return legandr(n - 1).mult((2*t - 1)/t).mult(new Polynom(Arrays.asList(new Double[]{0.0, 1.0}))).add(legandr(n - 2).mult((t - 1) / t));
    }
}
