package main;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Приближенное вычисление интеграла по составным квадратурным формулам
 */
public class Integral {
    public static void test(){
        Polynom polynom = new Polynom(Arrays.asList(new Double[]{1.0, 2.0, 3.0}));
        System.out.println("До интегрирования: " + polynom);
        Polynom integral = polynom.integrate();
        System.out.println("После интегрирования: " + integral);
        Scanner sc = new Scanner(System.in);
        double a = sc.nextDouble();
        double b = sc.nextDouble();
        int m = sc.nextInt();
        Segment segment = new Segment(a, b);
        double real = integral.getValue(segment.r) - integral.getValue(segment.l);
        System.out.println("Настоящее значение интеграла = " + real);
        List<IntegralType> types = Arrays.asList(new IntegralType[]{
                IntegralType.LEFT_RECTANGLE,
                IntegralType.RIGHT_RECTANGLE,
                IntegralType.AVERAGE_RECTANGLE,
                IntegralType.TRAPEZE,
                IntegralType.SIMPSON});
        for(IntegralType type : types){
            double value = integrate(polynom, segment, m, type);
            System.out.println(type + ":");
            System.out.println("    Значение = " + value);
            System.out.println("    Фактическая погрешность = " + Math.abs(value - real));
            System.out.println("    Теоретическая погрешность = " + error(segment, type));
        }
    }

    public static double integrate(Function function, Segment segment, int m, IntegralType type){
        Function w = (x) -> {return 1;};
        double value = 0;
        double h = segment.lenght() / m;
        for(int i = 0; i < m; i++)
            value += kf(function, new Segment(segment.l + h * i, segment.l + h * (i + 1)), type);
        return value;
    }

    private static double kf(Function function, Segment segment, IntegralType type){
        double h = segment.lenght();
        switch (type){
            case LEFT_RECTANGLE: return segment.lenght()*function.getValue(segment.l);
            case RIGHT_RECTANGLE: return segment.lenght()*function.getValue(segment.r);
            case AVERAGE_RECTANGLE: return segment.lenght()*function.getValue((segment.l + segment.r) / 2);
            case TRAPEZE: return segment.lenght()*(function.getValue(segment.l) + function.getValue(segment.l)) / 2;
            case SIMPSON: return segment.lenght()*(function.getValue(segment.l) +
                    4 * function.getValue((segment.l + segment.r) / 2) + function.getValue(segment.r)) / 6;
        }
        return 0;
    }

    private static String error(Segment segment, IntegralType type){
        double h = segment.lenght();
        switch (type){
            case LEFT_RECTANGLE: return Math.pow(segment.lenght(), 2) / 2 + " * c";
            case RIGHT_RECTANGLE: return Math.pow(segment.lenght(), 2) / 2 + " * c";
            case AVERAGE_RECTANGLE: return Math.pow(segment.lenght(), 3) / 24 + " * c";
            case TRAPEZE: return Math.pow(segment.lenght(), 3) / 12 + " * c";
            case SIMPSON: return Math.pow(segment.lenght(), 5) / 2880 + " * c";
        }
        return "0";
    }

    enum IntegralType{
        LEFT_RECTANGLE,
        RIGHT_RECTANGLE,
        AVERAGE_RECTANGLE,
        TRAPEZE,
        SIMPSON
    }
}
