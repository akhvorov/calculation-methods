package main;

/**
 * Created by Саша on 13.04.2016.
 */
public interface Function {
    double getValue(double x);
}