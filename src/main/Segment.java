package main;

/**
 * Created by Саша on 21.02.2016.
 */
public class Segment{
    public double l;
    public double r;

    public Segment(double a, double b){
        l = a;
        r = b;
    }

    public double lenght(){
        return r - l;
    }

    @Override
    public String toString(){
        return "[" + l + "; " + r + "]";
    }
}
