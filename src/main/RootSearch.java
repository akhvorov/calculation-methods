package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * Поиск корня
 * Метод половинного деление и метод Ньютона
 */
public class RootSearch {
    private Segment segment;
    private List<Segment> rootSegments;
    private List<Double> rootsBin;
    private List<Double> rootsNewton;
    private int N;
    private double A;
    private double B;
    private FunctionWithDerivative function;

    public static void test(){
        /*System.out.println("ЧИСЛЕННЫЕ МЕТОДЫ РЕШЕНИЯ НЕЛИНЕЙНЫХ АЛГЕБРАИЧЕСКИХ И ТРАНСЦЕНДЕНТНЫХ УРАВНЕНИЙ");
        System.out.println("x*x*x + 6*x*x - 90*x + 54 = 0");
        new RootSearch(new Function() {
            @Override
            public double getValue(double x) {
                double y;
                //y = x*x*x + 6*x*x - 90*x + 54;
                //y = Math.sin(x) + 3*x*x / 2;
                y = Math.sin(x);
                return y;
            }

            @Override
            public double derivative(double x) {
                double y;
                //y = 3*x*x + 12*x - 90;
                //y = Math.cos(x) + 3*x;
                y = Math.cos(x);
                return y;
            }
        }).Solve();*/
        //new DrawGraphics(new Segment(-10, 10));
        //System.out.println("end");
    }

    public RootSearch(FunctionWithDerivative function){
        this.function = function;
        rootSegments = new ArrayList<>();
        rootsBin = new ArrayList<>();
        rootsNewton = new ArrayList<>();
    }

    public void Solve(){
        firstInput();
        findSegments();
        printSegments();
        //rootSegments.forEach(this::findRoot);
        findRoots();
        //printRoots();
    }

    public List<Double> getRoots(Segment segment, int N){
        this.segment = segment;
        A = segment.l;
        B = segment.r;
        this.N = N;
        findSegments();
        findRootsDefault();
        return rootsBin;
    }

    private void firstInput(){
        System.out.println("Введите границы:");
        Scanner sc = new Scanner(System.in);
        A = sc.nextDouble();
        B = sc.nextDouble();
        if(A > B)
            swap(A, B);
        segment = new Segment(A, B);
        System.out.println("Ввидите N - число, на которое разбивать отрезок." + "\n"
         + "Если вы не знаете, введите 0");
        N = sc.nextInt();
        if(N <= 0)
            N = 1000;
    }

    private void findSegments(){
        double h = (B - A) / N;
        int k = 0, p = -1;
        while (k != p){
            //System.out.println(h + ", " + "k:p" + k + " " + p);
            p = k;
            k = 0;
            rootSegments.clear();
            for(double i = A; i <= B; i += h)
                if(function.getValue(i)*function.getValue(i + h) <= 0) {
                    rootSegments.add(new Segment(i, i + h));
                    k++;
                }
            N *= 10;
            h = (B - A) / N;
        }
    }

    private void printSegments(){
        System.out.println("Корни находятся в этих промежутках:");
        for(Segment seg : rootSegments)
            System.out.println(seg);
    }

    private void findRoots(){
        System.out.println("Введите точность");
        Scanner sc = new Scanner(System.in);
        //double d = Math.abs(sc.nextDouble());
        String s = sc.nextLine();
        double d = toDouble(s);
        System.out.println("d = " + d);
        System.out.println("Кратность корня. Если не хотите, все равно введите что-нибудь");
        int p = sc.nextInt();
        if(p < 1 || p > 10)
            p = 1;
        for(Segment segment : rootSegments){
            rootsBin.add(binarySearch(segment, d));
            //System.out.println("!");
            try {
                rootsNewton.add(newtown(segment, d, p));
            }
            catch (Exception e){
                System.out.println("Не смог найти корень в " + segment + " методом Ньютона");
            }
        }
    }

    private void findRootsDefault(){
        //double d = Math.abs(sc.nextDouble());
        double d = 0.0001;
        int p = 1;
        System.out.println(rootSegments);
        for(Segment segment : rootSegments)
            rootsBin.add(binarySearch(segment, d));
    }

    private void printRoots(){
        System.out.println("Метод бисекции: ");
        for(Double root : rootsBin)
            System.out.print(root + " ");
        System.out.println();
        System.out.println("Метод Ньтона: ");
        if(rootsNewton.isEmpty())
            System.out.println("Не нашел корней");
        else
            for(Double root : rootsNewton)
                System.out.print(root + " ");
    }

    private void findRoot(Segment segment){
        double l = segment.l;
        double r = segment.r;
        System.out.println("Input delta");
        Scanner sc = new Scanner(System.in);
        double d = Math.abs(sc.nextDouble());
        //binarySearch(l, r, d);
        //newtown(l, r, d);
    }

    private double binarySearch(Segment segment, double d){
        double l = segment.l, r = segment.r;
        double m = (l + r) / 2;
        int n = 0;
        while(Math.abs(r - l) > d){
            n++;
            if(function.getValue(l)*function.getValue(m) <= 0)
                r = m;
            else
                l = m;
            m = (r + l) / 2;
        }
        //outputRoot(segment.l, n, m, "бисекции");
        //rootsBin.add(m);
        return m;
    }

    private double newtown(Segment segment, double d, int p) throws Exception {
        double y, t;
        int n = 0;
        if(okNewtown(segment, d, p, segment.l))
            y = segment.l;
        else if(okNewtown(segment, d, p, segment.r))
            y = segment.r;
        else if(okNewtown(segment, d, p, (segment.r + segment.l) / 2))
            y = (segment.r + segment.l) / 2;
        else
            throw new Exception();
        double x = y + 5 * d;
        t = y;
        while(Math.abs(y - x) > d){
            n++;
            x = y;
            y = x - (p*function.getValue(x)/function.derivative(x));
        }
        outputRoot(t, n, y, "Ньютон");
        /*boolean end = false;
        boolean der = false;
        int count = 0;
        int k = 0;
        while(!end && count < 5){
            while(Math.abs(y - x) > d){
                x = y;
                if(Math.abs(derivative(x)) < 1) {
                    der = true;
                    break;
                }
                y = x + (p*function(x)/derivative(x));
                System.out.println(y);
            }
            if(der) {
                y = l + (r - l) * ran.nextGaussian();
                x = y + 100 * d;
                count++;
            }
            else{
                end = true;
                break;
            }
            k = 0;
            der = false;
        }*/
        return y;
    }

    private boolean okNewtown(Segment seg, double d, int p, double y){
        double x = y + 5*d;
        int k = 0;
        while(Math.abs(y - x) > d){
            x = y;
            k++;
            if(Math.abs(function.derivative(x)) < d || k > 20) {
                return false;
            }
            y = x - (p*function.getValue(x)/function.derivative(x));
            //System.out.println(y);
        }
        return true;
    }

    private void outputRoot(double start, int iterations, double root, String method){
        System.out.println(method + ": {Начальное приближение: " + start + "; кол-во итераций: " + iterations + "; приблизительный корень: " + root + "; невязка: " + Math.abs(function.getValue(root)) + "}");}


    private double toDouble(String s){
        double n = 0;
        int r = 10;
        int k = s.length();
        for(int i = 0; i < s.length(); i++)
            if(s.charAt(i) == ',' || s.charAt(i) == '.')
                k = i;
        for(int i = k + 1; i < s.length(); i++){
            n += (double)(s.charAt(i) - '0') / r;
            r *= 10;
        }
        r = 1;
        for(int i = k - 1; i >= 1; i--){
            n += (double)(s.charAt(i) - '0') * r;
            r *= 10;
        }
        if(s.charAt(0) == '-')
            n *= -1;
        else
            n += (double)(s.charAt(0) - '0') * r;
        return n;
    }

    private <T> void swap(T a, T b){
        T t = a;
        a = b;
        b = t;
    }
    /*public double function(double x){
        double y = x*x*x + 6*x*x - 90*x + 54;
        return y;
    }*/

    /*public double derivative(double x){
        double y = 3*x*x + 12*x - 90;
        return y;
    }*/
}
