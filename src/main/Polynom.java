package main;

import java.util.ArrayList;
import java.util.List;

/**
 * Многочлен
 */
public class Polynom implements FunctionWithDerivative {
    List<Double> coeff;

    public Polynom(){
        coeff = new ArrayList<Double>();
    }

    public Polynom(double d){
        coeff = new ArrayList<Double>();
        coeff.add(d);
    }

    public Polynom(List<Double> coeffs){
        coeff = coeffs;
    }

    public Polynom add(Polynom p){
        Polynom r = new Polynom();
        r.coeff.clear();
        //System.out.println(this + " +++ " + p);
        if(coeff.size() > p.coeff.size())
            for (int i = p.coeff.size(); i < coeff.size(); i++)
                p.coeff.add(0.0);
        else
            for (int i = coeff.size(); i < p.coeff.size(); i++)
                coeff.add(0.0);

        for (int i = 0; i < coeff.size(); i++)
            r.coeff.add(coeff.get(i) + p.coeff.get(i));
        return r;
    }

    public Polynom add(double d){
        Polynom p = new Polynom(d);
        Polynom r = new Polynom();
        r.coeff.clear();
        //System.out.println(this + " +++ " + p);
        if(coeff.size() > p.coeff.size())
            for (int i = p.coeff.size(); i < coeff.size(); i++)
                p.coeff.add(0.0);
        else
            for (int i = coeff.size(); i < p.coeff.size(); i++)
                coeff.add(0.0);

        for (int i = 0; i < coeff.size(); i++)
            r.coeff.add(coeff.get(i) + p.coeff.get(i));
        return r;
    }

    public Polynom mult(Polynom p){
        Polynom r = new Polynom();
        r.coeff.clear();
        int t = p.coeff.size() + coeff.size() - 1;
        for(int i = 0; i < t; i++)
            r.coeff.add(0.0);
        for(int i = 0; i < coeff.size(); i++)
            for (int j = 0; j < p.coeff.size(); j++)
                r.coeff.set(i + j, r.coeff.get(i + j) + coeff.get(i)*p.coeff.get(j));
        return r;
    }

    public Polynom mult(double d){
        Polynom r = new Polynom();
        r.coeff.clear();
        for(int i = 0; i < coeff.size(); i++)
            r.coeff.add(coeff.get(i) * d);
        return r;
    }

    public Polynom div(double d){
        Polynom p = new Polynom();
        p.coeff.clear();
        for (int i = 0; i < coeff.size(); i++)
            p.coeff.add(coeff.get(i) / d);
        return p;
    }

    public double getValue(double d){
        double s = 0.0;
        double r = 1.0;
        for(int i = 0; i < coeff.size(); i++){
            s += coeff.get(i) * r;
            r *= d;
        }
        return s;
    }

    public int deg(){
        return coeff.size() - 1;
    }

    public Polynom der(){
        Polynom p = new Polynom();
        p.coeff.clear();
        for (int i = 1; i < coeff.size(); i++)
            p.coeff.add(coeff.get(i) * i);
        return p;
    }

    public double derivative(double x){
        return der().getValue(x);
    }

    public Polynom integrate(){
        Polynom p = new Polynom();
        p.coeff.clear();
        p.add(0);
        for (int i = 0; i < coeff.size(); i++)
            p.coeff.add(coeff.get(i) / (i + 1));
        return p;
    }

    public List<Double> roots(){
        RootSearch rootSearch = new RootSearch(new Polynom(coeff));
        return rootSearch.getRoots(new Segment(-10, 10), 20000);
    }

    public String allString(){
        String s = "";
        if(!coeff.isEmpty()) {
            s = coeff.get(0).toString();
            if (coeff.get(0) > 0)
                s = "+ " + s;
            /*else
                s = "-" + s;*/
        }
        if(coeff.size() > 1) {
            s = coeff.get(1).toString() + "*x " + s;
            if (coeff.get(1) > 0)
                s = "+ " + s;
            /*else
                s = "-" + s;*/
        }
        for(int i = 2; i < coeff.size(); i++) {
            s = "x^" + i + " " + s;
            if (!coeff.get(i).equals(1.0))
                s = coeff.get(i) + "*" + s;
            if (coeff.get(i) > 0)
                s = "+ " + s;
            /*else
            s = "-" + s;*/
        }
        return s;
    }

    @Override
    public String toString(){
        String s = "";
        if(!coeff.isEmpty() && !coeff.get(0).equals(0.0)) {
            s = coeff.get(0).toString();
            if (coeff.get(0) > 0)
                s = "+ " + s;
            /*else
                s = "-" + s;*/
        }
        if(coeff.size() > 1 && !coeff.get(1).equals(0.0)) {
            s = coeff.get(1).toString() + "*x " + s;
            if (coeff.get(1) > 0)
                s = "+ " + s;
            /*else
                s = "-" + s;*/
        }
        for(int i = 2; i < coeff.size(); i++)
            if(!coeff.get(i).equals(0.0)) {
                s = "x^" + i + " " + s;
                if (!coeff.get(i).equals(1.0))
                    s = coeff.get(i) + "*" + s;
                if (coeff.get(i) > 0)
                    s = "+ " + s;
                /*else
                    s = "-" + s;*/
            }
        return s;
    }
}
