package main;

/**
 * Created by Саша on 21.02.2016.
 */
public interface FunctionWithDerivative extends Function {
    double derivative(double x);
}
